[![pipeline status](https://git.coop/aptivate/ansible-roles/nagios-nrpe/badges/master/pipeline.svg)](https://git.coop/aptivate/ansible-roles/nagios-nrpe/commits/master)

# nagios-nrpe

Nagios NRPE support

# Requirements

None.

# Role Variables

* `nagios_nrpe_allowed_hosts`: The Nagios host which is allowed to make requests.
  * Default is our Aptivate specific Nagios host IP.

# Dependencies

* https://git.coop/aptivate/ansible-roles/epel-repository

# Example Playbook

```yaml
- hosts: localhost
  roles:
     - role: nagios-nrpe
```

# Testing

```bash
$ pipenv install --dev
$ pipenv run molecule test
```

# License

  * https://www.gnu.org/licenses/gpl-3.0.en.html

# Author Information

  * https://aptivate.org/
  * https://git.coop/aptivate
